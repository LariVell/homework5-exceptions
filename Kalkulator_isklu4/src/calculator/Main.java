package calculator;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        try {
            System.out.print("Введите первое число:");
            double a = in.nextDouble();
            System.out.print("Введите второе число:");
            double b = in.nextDouble();

        Number number = new Number();
        System.out.println("Сумма -  " + number.getPlus(a, b));

        System.out.println("Разность -  " + number.getMinus(a, b));

        try {

            if (b == 0) {
                throw new ArithmeticException();
            }
            else {
                System.out.println("Частное -  " + number.getDivision(a, b));
            }
        }
        catch (ArithmeticException e) {
            System.out.println("Частное - Делить на ноль нельзя!");
        }

        System.out.println("Произведени -  " + number.getMultiplication(a, b));
        }
        catch (InputMismatchException e) {
            System.out.println("Неправильный ввод!");
        }
    }
}
